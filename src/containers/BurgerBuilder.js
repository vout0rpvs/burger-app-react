import React, { Component } from 'react';
import Aux from '../hoc/Aux'
import Burger from '../components/Burger/Burger'
import BuildControls from '../components/Burger/BuildControls/BuildControls'
import Modal from '../components/UI/Modal/Modal'
import OrderSummary from '../components/Burger/OrderSummary/OrderSummary'
import axios from '../axios-orders'
import Spinner from '../components/UI/Spinner/Spinner'
import withErrorHandler from "../hoc/withErrorHandler/withErrorHandler";
const INGREDIENT_PRICES = {
    salad: 0.5,
    cheese: 0.9,
    meat: 3,
    bacon:0.3
}
class BurgerBuilder extends Component {
    constructor(props){
        super(props);
        this.state = {
            ingredients:null,
            totalPrice: 4,
            purchasable: false,
            checkout: false,
            loading:false
        }
    }
    componentDidMount(){
        axios.get('/Ingredients.json')
        .then(response => {
            this.setState({ingredients:response.data})
            console.log(response);
        })
        .catch(error => {
            this.setState({error:true})
        })
    }
    updatePurchaseState = ingredients => {
        const sum = Object.keys(ingredients)
        .map(igKey => {
            return ingredients[igKey]
        })
        .reduce((sum,el)=>{
            return sum + el;
        },0)
        console.log(ingredients);
        
        this.setState({purchasable: sum > 0});
    }
    purchaseHandler = () => {
        this.setState({checkout:true})
    }
    purchaseCancelHandler = () => {
        this.setState({checkout:false})
    }
    purchaseContinueHandler = () => {
        this.setState({loading:true})
        const order = {
            ingredients: this.state.ingredients,
            price:this.state.totalPrice,
            customer: {
                name: 'Oleh Fedoryshyn',
                address: {
                    street: 'Symonenka',
                    zipCode: '41351',
                    country: 'Ukraine'
                },
                email:'vout0rpvs@gmail.com'
            },
            deliveryMethod: 'fastest'
        }
        axios.post('/orders.json', order).then(response => {
            this.setState({loading:false, checkout:false})
        }).catch(error =>  
            this.setState({loading:false, checkout:false})
        );
    }
    addIngredientHandler = type => {
        const oldCount= this.state.ingredients[type]
        
        if(oldCount < 0){
            return;
        }
        const updatedIngredients = {
            ...this.state.ingredients
        }
        updatedIngredients[type] = this.state.ingredients[type] + 1;
        const newPrice = this.state.totalPrice + INGREDIENT_PRICES[type];
        this.setState({totalPrice: newPrice, ingredients:updatedIngredients});
        this.updatePurchaseState(updatedIngredients);
    }
    removeIngredientHandler = type => {
        const oldCount= this.state.ingredients[type]
        if(oldCount < 0){
            return;
        }
        const updatedIngredients = {
            ...this.state.ingredients
        }
        updatedIngredients[type]= oldCount - 1;
        const oldPrice = this.state.totalPrice;
        const newPrice = oldPrice - INGREDIENT_PRICES[type];
        this.setState({totalPrice: newPrice, ingredients:updatedIngredients});
        this.updatePurchaseState(updatedIngredients);
    }

    render(){
        const disabledInfo = {
            ...this.state.ingredients
        };
        for(let key in disabledInfo){
            disabledInfo[key] = disabledInfo[key] <= 0;  
        }
        let orderSummary = null;
        let burger = this.state.error ? <p style={{textAlign:'center'}}>Ingredients can't be loaded</p> : <Spinner />

        if(this.state.ingredients){
            burger = (
                <Aux>
                <Burger ingredients={this.state.ingredients}/>
                <BuildControls 
                    ingredientAdded={this.addIngredientHandler}
                    ingredientRemoved={this.removeIngredientHandler}
                    disabled={disabledInfo}
                    price={this.state.totalPrice}
                    purchasable={this.state.purchasable}
                    checkout={this.purchaseHandler}/>
                </Aux>)

                 orderSummary =  <OrderSummary 
                 price={this.state.totalPrice}
                 ingredients={this.state.ingredients} 
                 modalClosed={() => this.purchaseCancelHandler()}
                 modalContinue={()=> this.purchaseContinueHandler()}/>
        }
        if(this.state.loading){
            orderSummary = <Spinner />
        }
        return(
            <Aux>
                <Modal show={this.state.checkout} modalClosed={this.purchaseCancelHandler}>
                   {orderSummary}
                </Modal>
                    {burger}
                
            </Aux>
        );
    }
}
export default withErrorHandler(BurgerBuilder, axios)