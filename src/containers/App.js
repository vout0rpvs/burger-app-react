import React, { Component } from 'react';

import Layout from '../hoc/Layout/Layout'
import BurgerBuilder from "../containers/BurgerBuilder";
import Checkout from './Checkout/Checkout'
import { Route } from 'react-router-dom'

class App extends Component {
  render() {
    return (
      <div>
        <Layout>
         <Route path="/checkout" component={Checkout}/>
         <Route path="/" exact component={BurgerBuilder}/>
        </Layout>
      </div>
    );
  }
}

export default App;
